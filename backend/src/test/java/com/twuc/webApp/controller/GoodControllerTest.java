package com.twuc.webApp.controller;

import com.twuc.webApp.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;


import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class GoodControllerTest extends ApiTestBase {

    private ResultActions addApple() throws Exception {
        return mockMvc.perform(put("/api/goods")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{" +
                        "\"name\": \"苹果\"," +
                        "\"price\": \"3.00\"," +
                        "\"image\": \"https://s2.ax1x.com/2019/10/14/uz5jtf.jpg\"," +
                        "\"unit\": \"斤\"}")
        );
    }
    @Test
    void should_add_good() throws Exception {
        addApple().andExpect(status().is(200));
    }


    @Test
    void should_get_goods_list() throws Exception {
        addApple();
        addApple();
        mockMvc.perform(get("/api/goods")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().is(200)).andExpect(content()
                .json("[{\"id\":1,\"name\":\"苹果\",\"price\":3.0,\"image\":\"https://s2.ax1x.com/2019/10/14/uz5jtf.jpg\",\"unit\":\"斤\"}," +
                        "{\"id\":2,\"name\":\"苹果\",\"price\":3.0,\"image\":\"https://s2.ax1x.com/2019/10/14/uz5jtf.jpg\",\"unit\":\"斤\"}]"));
    }

    //    @Test
//    void should_add_a_staff() throws Exception {
//        mockMvc.perform(post("/api/staffs")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content("{\"firstName\": \"Rob\",\"lastName\": \"Hall\"}")
//        ).andExpect(status().is(200)).andExpect(header().string("Location",startsWith("/api/staffs/")));
//    }
//
//    @Test
//    void should_return_400_when_add_illegal_staff_info() throws Exception {
//        mockMvc.perform(post("/api/staffs")
//                .contentType(MediaType.APPLICATION_JSON)
//                .content("{\"lastName\": \"Hall\"}")
//        ).andExpect(status().is(400));
//    }
//
//    @Test
//    void should_get_a_staff() throws Exception {
//        MvcResult mvcResult = mockMvc.perform(post("/api/staffs")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content("{\"firstName\": \"Rob\",\"lastName\": \"Hall\"}")
//        ).andReturn();
//        String url = mvcResult.getResponse().getHeader("Location");
//        mockMvc.perform(get(url)).andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$.firstName", is("Rob")))
//                .andExpect(jsonPath("$.lastName", is("Hall")));
//    }
//
//    @Test
//    void should_return_404_when_get_staff_id_not_exists() throws Exception {
//        mockMvc.perform(get("/api/staffs/1")).andExpect(status().isNotFound());
//    }
//
//    @Test
//    void should_get_all_staffs() throws Exception {
//        mockMvc.perform(get("/api/staffs")).andExpect(status().isOk())
//                .andExpect(content().json("[]"));
//        mockMvc.perform(post("/api/staffs")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content("{\"firstName\": \"Rob\",\"lastName\": \"Hall\"}")
//        );
//        mockMvc.perform(post("/api/staffs")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content("{\"firstName\": \"Wang\",\"lastName\": \"Ming\"}")
//        );
//        mockMvc.perform(get("/api/staffs")).andExpect(status().isOk())
//                .andExpect(content().json("[{id:1, firstName : Rob, lastName: Hall}, {id:2, firstName: Wang, lastName: Ming}]"));
//    }
//
//    @Test
//    void should_set_timeZone() throws Exception {
//        mockMvc.perform(post("/api/staffs")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content("{\"firstName\": \"Rob\",\"lastName\": \"Hall\"}")
//        );
//        mockMvc.perform(put("/api/staffs/1/timezone").contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content("{\"zoneId\":\"Asia/Chongqing\"}"))
//                .andExpect(status().isOk());
//    }
//
//    @Test
//    void should_return_400_when_use_wrong_property_name() throws Exception {
//        mockMvc.perform(post("/api/staffs")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content("{\"firstName\": \"Rob\",\"lastName\": \"Hall\"}")
//        );
//        mockMvc.perform(put("/api/staffs/1/timezone").contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content("{\"zoneID\":\"Asia/Chongqing\"}"))
//                .andExpect(status().is(400));
//    }
//
//    @Test
//    void should_get_zoneId() throws Exception {
//        MvcResult mvcResult = mockMvc.perform(post("/api/staffs")
//                .contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content("{\"firstName\": \"Rob\",\"lastName\": \"Hall\"}")
//        ).andReturn();
//        String url = mvcResult.getResponse().getHeader("Location");
//        mockMvc.perform(get(url)).andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$.firstName", is("Rob")))
//                .andExpect(jsonPath("$.lastName", is("Hall")))
//                .andExpect(jsonPath("$.zoneId", nullValue()));
//        mockMvc.perform(put("/api/staffs/1/timezone").contentType(MediaType.APPLICATION_JSON_UTF8)
//                .content("{\"zoneId\":\"Asia/Chongqing\"}"));
//        mockMvc.perform(get(url)).andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//                .andExpect(jsonPath("$.firstName", is("Rob")))
//                .andExpect(jsonPath("$.lastName", is("Hall")))
//                .andExpect(jsonPath("$.zoneId",is("Asia/Chongqing")));
//    }
}
