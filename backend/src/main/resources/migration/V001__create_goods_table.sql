set character_set_server=utf8;
set character_set_database=utf8;
CREATE TABLE IF NOT EXISTS good(
    `id`            BIGINT              AUTO_INCREMENT PRIMARY KEY,
    `name`          varchar(128)        NOT NULL,
    `price`         DOUBLE             NOT NULL,
    `image`         varchar(256)        ,
    `unit`          varchar(32)         NOT NULL
)DEFAULT CHARSET = utf8;
