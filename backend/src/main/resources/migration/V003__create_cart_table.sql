CREATE TABLE IF NOT EXISTS Cart(
    `id`        BIGINT             AUTO_INCREMENT PRIMARY KEY,
    `num`       INTEGER             NOT NULL,
    `good_id`    BIGINT             NOT NULL,
     FOREIGN KEY (good_id) REFERENCES good(id)
)DEFAULT CHARSET = utf8;