package com.twuc.webApp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Good {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    @NotNull
    @Size(max=128)
    private String name;

    @Column
    @NotNull
    @Min(0)
    private Double price;

    @Column
    @Size(max=256)
    private String image;

    @Column
    @Size(max=32)
    @NotNull
    private String unit;


    public Good() {
    }

    public Good(String name, Double price, String image, String unit) {
        this.name = name;
        this.price = price;
        this.image = image;
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
