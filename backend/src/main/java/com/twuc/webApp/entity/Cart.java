package com.twuc.webApp.entity;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Cart {
    @Id
    @GeneratedValue
    private Long id;

    @Column
    @NotNull
    @Min(0)
    private Integer num;

    @ManyToOne
    private Good good;

    public Cart() {
    }

    public Cart(Integer num, Good good) {
        this.num = num;
        this.good = good;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Good getGood() {
        return good;
    }

    public void setGood(Good good) {
        this.good = good;
    }
}
