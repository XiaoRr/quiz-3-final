package com.twuc.webApp.controller;


import com.twuc.webApp.entity.Good;
import com.twuc.webApp.repository.GoodRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@CrossOrigin
public class GoodController {

    private GoodRepository goodRepository;

    public GoodController(GoodRepository staffRepository) {
        this.goodRepository = staffRepository;
    }

    @PutMapping("/api/goods")
    public ResponseEntity addGood(@Valid @RequestBody Good good){
        Optional<Good> goodByName = goodRepository.getGoodByName(good.getName());
        if(goodByName.isPresent()){
            return ResponseEntity.status(400).build();
        }
        Good savedGood = goodRepository.save(good);
        return ResponseEntity.status(200).build();
    }

    @GetMapping("api/goods/{id}")
    public ResponseEntity getStaff(@PathVariable Long id){
        Optional<Good> good = goodRepository.findById(id);
        if(good.isEmpty()){
            return ResponseEntity.status(404).build();
        }
        return ResponseEntity.status(200).body(good);
    }

    @GetMapping("api/goods")
    public ResponseEntity getGoods(){
        return ResponseEntity.status(200).body(goodRepository.findAll());
    }
}
