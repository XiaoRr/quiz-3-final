package com.twuc.webApp.repository;

import com.twuc.webApp.entity.Good;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface GoodRepository extends JpaRepository<Good,Long> {

    public Optional<Good> getGoodByName(String name);
}
