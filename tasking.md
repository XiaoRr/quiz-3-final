1. 项目搭建
    1. 前端项目搭建
        1. 项目依赖配置
        1. 运行
    1. 后端项目搭建
         1. 项目依赖配置
         1. 运行   
1. 后端代码编写
    1. 商品
        1. 获得所有商品列表
        1. 添加商品
    1. 订单
        1. 添加订单
        1. 获取订单列表
        1. 删除订单
1. 前端代码编写
    1. 商品
        1. 显示所有商品列表
        1. 添加商品
    1. 订单
        1. 添加订单
        1. 获取订单列表
        1. 删除订单
    1.前端页面美化
    
        
    