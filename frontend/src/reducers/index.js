import {combineReducers} from "redux";
import homeReducer from "../app/Home/reducers/homeReducer";

const reducers = combineReducers({
  homeReducer:homeReducer
});
export default reducers;