import React, {Component} from 'react';
import Axios from "axios";

export default class GoodPage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name : null,
      price : null,
      unit : null,
      image : null,
      nameChecker : "",
      unitChecker : "",
      imageChecker : "",
      numberChecker : ""
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handlePriceChange = this.handlePriceChange.bind(this);
    this.handleUnitChange = this.handleUnitChange.bind(this);
    this.handleImageChange = this.handleImageChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }

  handleNameChange (event) {
    this.setState({name: event.target.value});
    this.setState({nameChecker: event.target.value !== ""});
  }
  handlePriceChange (event) {
    this.setState({price: event.target.value});
    this.setState({numberChecker: /^\d+(\.\d+)?$/.test(event.target.value)});
  }
  handleUnitChange (event) {
    this.setState({unit: event.target.value});
    this.setState({unitChecker: event.target.value !== ""});
  }
  handleImageChange (event) {
    this.setState({image: event.target.value});
    this.setState({imageChecker: event.target.value !== ""});
  }
  handleSubmit () {
    Axios.put("http://localhost:8080/api/goods",this.state).then((response => {
        alert("创建成功");
    })).catch(
      () => {alert("商品名称已存在，请输入新的商品名称");}
    ).finally(
      () => {document.getElementById("name").select();}
    )
  }

  componentDidMount() {
    document.getElementById("submit").disabled =
      !(this.state.nameChecker && this.state.imageChecker && this.state.unitChecker && this.state.numberChecker);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    document.getElementById("submit").disabled =
      !(this.state.nameChecker && this.state.imageChecker && this.state.unitChecker && this.state.numberChecker);
  }

  render() {
    return (
      <div className="addGood">
        <h1>添加商品</h1>
        <div className={"text"}>名称：</div>
        <input type={"text"} id={"name"} placeholder={"名称"} onChange={this.handleNameChange}/>
        <div className={"text"}>价格：</div>
        <input type={"number"} id={"price"} step={"0.01"} min={0} placeholder={"价格"} onChange={this.handlePriceChange}/>
        <div className={"text"}>单位：</div>
        <input type={"text"} id={"unit"} placeholder={"单位"} onChange={this.handleUnitChange}/>
        <div className={"text"}>图片：</div>
        <input type={"text"} id={"image"} placeholder={"图片"} onChange={this.handleImageChange}/>
        <br/>
        <button id={"submit"} onClick={this.handleSubmit}>提交</button>
      </div>
    );
  }
}
