
const initState = {
  goods: []
};

const homeReducer = (state = initState, action) => {
  switch (action.type) {
    case "GET_ALL_GOODS":
      return {
        ...state,
        goods:action.goods
      };
    default:
      return state
  }
};

export default homeReducer;
