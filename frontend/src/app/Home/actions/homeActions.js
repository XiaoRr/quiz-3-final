import Axios from "axios";

export const getAllGoods = () =>{
  return (dispatch) => {
    Axios.get("http://localhost:8080/api/goods")
      .then((res) =>{
        dispatch({type: "GET_ALL_GOODS", goods: res.data});
        console.log("async get",res.data)
      })
  }
};