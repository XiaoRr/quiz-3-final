import React, {Component} from 'react';
import {connect} from "react-redux";
import {getAllGoods} from '../actions/homeActions';
import {bindActionCreators} from "redux";
import Axios from "axios";


class HomePage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      clickable:true
    };
    this.addToCart = this.addToCart.bind(this);
    this.good = this.good.bind(this);
  }

  componentDidMount() {
    this.props.getAllGoods();
  }
  addToCart(id) {
    return () => {
      Axios.put("http://localhost:8080/api/carts",{id:id}).then(()=>{
        this.setState({clickable:true});
      })
      this.setState({clickable:false})
    }
  }
  renderGoodList(goods) {
    console.log("goods",goods);
    if(goods != null)
      return goods.map(this.good)
  }

  render() {
    const {goods} = this.props;
    return (
      <div className="">
        {this.renderGoodList(goods,this.state.clickable,this.addToCart)}>
      </div>
    );
  }

  good(good,clickable,cart) {
    return <div className={"goodView"}>
      <img src={good.image}/>
      <div className={"itemName"}>{good.name}</div>
      <div className={"itemName"}>单价：{good.price}元/{good.unit}</div>
      <button disabled={!clickable} onClick={this.addToCart(good.id)}>+</button>
    </div>
  }

}

const mapStateToProps = state => ({
  goods: state.homeReducer.goods
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAllGoods: getAllGoods
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
