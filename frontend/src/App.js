import React, {Component} from 'react';
import './App.less';
import HomePage from "./app/Home/pages/HomePage";
import {BrowserRouter as Router,NavLink} from "react-router-dom";
import {Route, Switch} from "react-router";
import GoodPage from "./app/AddGoods/pages/GoodPage";


class App extends Component{

  constructor(props){
    super(props);
  }

  render() {
    return (
      <div className='App'>
        <Router>
          <header>
            <nav>
              <NavLink activeClassName = "activeNav" exact to = "/">商城</NavLink>
              <NavLink activeClassName = "activeNav" exact to = "/orders">订单</NavLink>
              <NavLink activeClassName = "activeNav" exact to = "/addGoods">添加商品</NavLink>
            </nav>
          </header>
          <Switch>
            <Route path="/" exact component={HomePage}/>
            <Route path="/addGoods" exact component={GoodPage}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;